import { Model } from "../../infrastructure/Model";
import { Entity } from "../../infrastructure/Entity";
import { Util } from "../../common/Util";


@Model(process.env.DYNAMODB_PROJECT_TABLE)
export class Project extends Entity {

    private _name: string;
    private _searchName: string;
    private _redirectUri: string;
    private _secret: string;
    
    constructor(){super();}

    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get redirectUri(): string {
        return this._redirectUri;
    }

    set redirectUri(redirectUri: string) {
        this._redirectUri = redirectUri;
    }

    get secret(): string {
        return this._secret;
    }

    set secret(secret: string) {
        this._secret = secret;
    }

    get searchName(): string {
        return this._searchName;
    }

    set searchName(searchName: string) {
        this._searchName = searchName;
    }

    static deserialize(serialized: string) {

        const {id, name, redirectUri, secret} = JSON.parse(serialized);
        let project: Project =  new Project();
        project.name = name;
        project.searchName = Util.removeSpecialChar(name);
        project.redirectUri = redirectUri;
        project.secret = secret;
        project.id = id;
            
        return project;
    }
}