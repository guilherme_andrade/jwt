'use strict';
import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { VerifyExistsProject } from './../specification/VerifyExistsProject';
import { Project } from '../models/Project';
import { ProjectRepository } from '../../repositories/ProjectRepository';
import { Response } from '../../infrastructure/Response';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  let project: Project = Project.deserialize(event.body);
  if (!project.name || !project.redirectUri || !project.secret) {
    console.error('Validation Failed');
    let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
    cb(null, resp);
    return;
  }
  const projectRepository: ProjectRepository = new ProjectRepository();

  projectRepository.findById(project).then((response: Response) => {
    let projectOld: Project = JSON.parse(response.body);

    VerifyExistsProject.verify(project.name).then((projectVerified: Project) => {
      if(projectVerified && (projectVerified.name !== projectOld.name)) {
        let resp: Response = {statusCode: 401, body: JSON.stringify({message: 'Unauthorized'})};
        cb(null, resp);
      } else {
        projectRepository.update(project).then((resp: Response) => {
          cb(null, resp);
        }).catch((resp: Response)=>{
          cb(null, resp);
        });
      }
    });
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
  
};

export {handler};
