'use strict';
import { FilterExpressionBuilder } from './../../dynamodb/FilterExpressionBuilder';

import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { Project } from '../models/Project';
import { ProjectRepository } from '../../repositories/ProjectRepository';
import { Condition } from '../../dynamodb/Condition';
import { Response } from '../../infrastructure/Response';

const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  let filterExpressionBuilder = new FilterExpressionBuilder(process.env.DYNAMODB_PROJECT_TABLE, 10);
  let queryStringParameters = event.queryStringParameters;
  let exclusiveStartKey : {} = null;
  if(event.queryStringParameters) {
    if(!queryStringParameters.exclusiveStartKey) {
      filterExpressionBuilder.where('searchName', queryStringParameters.searchName, Condition.CONTAINS)
      filterExpressionBuilder.where('redirectUri', queryStringParameters.redirectUri, Condition.CONTAINS);
    } else {
      filterExpressionBuilder.exclusiveStartKey = queryStringParameters.exclusiveStartKey;
    }
  }
  
  const projectRepository: ProjectRepository = new ProjectRepository();
  projectRepository.findByQueryParameters(filterExpressionBuilder).then((resp: Response) => {
    cb(null, resp);
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
};

export {handler};
