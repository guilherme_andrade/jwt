'use strict';

import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { Project } from '../models/Project';
import { Response } from '../../infrastructure/Response';
import { ProjectRepository } from '../../repositories/ProjectRepository';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  if (!event.pathParameters.id) {
    console.error('Validation Failed');
    let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
    cb(null, resp);
    return;
  }

  const project: Project = Project.deserialize(`{"id": "${event.pathParameters.id}"}`);
  const projectRepository: ProjectRepository = new ProjectRepository();
  projectRepository.findById(project).then((resp: Response) => {
    cb(null, resp);
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
};

export {handler};
