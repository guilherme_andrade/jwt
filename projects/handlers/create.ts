'use strict';

import { VerifyExistsProject } from './../specification/VerifyExistsProject';
import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { Project } from '../models/Project';
import { ErrorDynamoDB } from '../../dynamodb/ErrorDynamoDB';
import { v4 as uuid } from 'uuid';
import { ProjectRepository } from '../../repositories/ProjectRepository';
import { Response } from '../../infrastructure/Response';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

    console.log(event);
    let project: Project = Project.deserialize(event.body);
    if (!project.name || !project.redirectUri) {
      console.error('Validation Failed');
      let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
      cb(null, resp);
      return;
    }

    VerifyExistsProject.verify(project.name).then((projectVerified: Project) => {
      if(projectVerified) {
        let resp: Response = {statusCode: 401, body: JSON.stringify({message: 'Unauthorized'})};
        cb(null, resp);
      } else {
        project.secret = uuid();
        const projectRepository: ProjectRepository = new ProjectRepository();
        projectRepository.add(project).then((resp: Response) => {
          cb(null, resp);
        }).catch((resp: Response)=>{
          cb(null, resp);
        });
      }
    });
  
};

export {handler};
