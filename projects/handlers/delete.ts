'use strict';

import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { Project } from '../models/Project';
import { ErrorDynamoDB } from '../../dynamodb/ErrorDynamoDB';
import { ProjectRepository } from '../../repositories/ProjectRepository';
import { Response } from '../../infrastructure/Response';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  if (!event.pathParameters.id) {
    console.error('Validation Failed');
    let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
    cb(null, resp);
    return;
  }

  const project: Project = Project.deserialize(`{"id": "${event.pathParameters.id}"}`);
  const projectRepository: ProjectRepository = new ProjectRepository();
  projectRepository.remove(project).then((resp: Response) => {
    cb(null, resp);
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
};

export {handler};
