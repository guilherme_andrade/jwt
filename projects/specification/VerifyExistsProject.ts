import { KeyConditionExpressionBuilder } from './../../dynamodb/KeyConditionExpressionBuilder';
import { Project } from './../models/Project';
import { ScanInput, DocumentClient } from 'aws-sdk/clients/dynamodb';
import { Condition } from './../../dynamodb/Condition';
import { Callback } from 'aws-lambda';
import { ProjectRepository } from '../../repositories/ProjectRepository';
import { Response } from '../../infrastructure/Response';

export class VerifyExistsProject {

    static verify(name: string) : Promise<Project>{
        return new Promise<Project>((resolve, reject) => {
            let keyConditionExpressionBuilder = new KeyConditionExpressionBuilder('nameIndex', process.env.DYNAMODB_PROJECT_TABLE)
            keyConditionExpressionBuilder.where('searchName', name, Condition.EQ);

            const projectRepository: ProjectRepository = new ProjectRepository();
            projectRepository.findByIndex(keyConditionExpressionBuilder).then((resp: Response) => {
                let output: DocumentClient.QueryOutput = JSON.parse(resp.body);
                let project: Project = output.Count > 0 ? <Project>output.Items[0]: null;
                resolve(project);
              }).catch((resp: Response)=>{
                reject(resp);
              });

        });
    }

}