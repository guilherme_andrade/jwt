'use strict';
import { User } from './../../users/models/User';
import * as JWT from 'jsonwebtoken';
import { Handler, CustomAuthorizerEvent, Context, Callback, PolicyDocument, Statement, AuthResponse, AuthResponseContext} from "aws-lambda";
import { Response } from '../../infrastructure/Response';

/* TODO
Pesquisar projeto pelo id passado no Headers
Com a a secret do projeto verificar o token do usuário
Verificar se a ação requisitada está no scopo de permissões do usuário */
const handler: Handler = (event: CustomAuthorizerEvent, context: Context, cb: Callback) => {
    console.log('init authorization... ');
    const authorization: string = event.authorizationToken;
    try {
        console.log('before verifying jwt');
        const decodedUser: User = <User>JWT.verify(authorization.replace('JWT ', ''), process.env.JWT_SECRET);
        console.log('jwt verified');
        const userId: string = decodedUser.id;
        const statement: Statement = {Action: 'execute-api:Invoke', Effect: 'Allow', Resource: event.methodArn};
        const policyDocument: PolicyDocument = {Version: '2012-10-17', Statement: [statement]};
        const authorizerContext: AuthResponseContext = { user: JSON.stringify(decodedUser) };
        const authResponse: AuthResponse = {principalId: userId, policyDocument: policyDocument, context: authorizerContext};
        console.log('calling callback');
        
        cb(null, authResponse);
    } catch(error) {
        let resp: Response = {statusCode: 401, body: JSON.stringify({message: 'Unauthorized'})};
        cb(null, resp);
    }
    //TODO verificar scope

};

export {handler};
