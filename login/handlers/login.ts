'use strict';
import { User } from './../../users/models/User';
import * as JWT from 'jsonwebtoken';
import { Handler, APIGatewayEvent, Context, Callback } from "aws-lambda";
import { Response } from '../../infrastructure/Response';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
    let user: User = User.deserialize(event.body);
    
    if (!user || !user.email || !user.password) {
        console.error('Validation Failed');
        let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
        cb(null, resp);
        return;
    }

    let signOptions: JWT.SignOptions = {expiresIn: process.env.JWT_EXPIRATION_TIME}
    user.login(user.email, user.password).then((returnedUser) => {
        let token: string  = JWT.sign(returnedUser, process.env.JWT_SECRET, signOptions);
        let resp: Response = {statusCode: 200, body: JSON.stringify({token: `JWT ${token}`})};
        cb(null, resp);
    }).catch((error: Error)=>{
        let resp: Response = {statusCode: 401, body: JSON.stringify({message: 'Unauthorized'})};
        cb(null, resp);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 , resp);
    });
};

export {handler};
