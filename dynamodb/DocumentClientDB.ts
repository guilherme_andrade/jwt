'use strict';
import { ErrorDynamoDB } from './ErrorDynamoDB';
import { DynamoDB } from 'aws-sdk';
import { DocumentClient, AttributeValue, Key, ScanInput, ExpressionAttributeValueMap, ExpressionAttributeNameMap } from 'aws-sdk/clients/dynamodb';
import { Response } from '../infrastructure/Response';
import * as co from 'co';

export class DocumentClientDB {
    
    static getDocumentClient() : DocumentClient {
        let documentClient : DocumentClient = null;
        let dynamodbOfflineOptions = {
            region: "localhost",
            endpoint: "http://localhost:8000",
            accessKeyId: "localAccessKey",
            secretAccessKey: "localSecretAccessKey"
        };
        
        let isOffline: boolean = process.env.IS_OFFLINE === 'true';
        if(isOffline) {
            documentClient = new DynamoDB.DocumentClient(dynamodbOfflineOptions);
        } else {
            documentClient = new DynamoDB.DocumentClient();
        }

        return documentClient;
    }

    static update (tableName: string, object: {}): Promise<Response> {
        const params = {
            TableName: tableName,
            Item: object,
            ReturnValues: 'ALL_OLD'
        };
        console.log(params);
        return new Promise<Response>((resolve, reject) => {
            DocumentClientDB.getDocumentClient().put(params).promise()
            .then((value: DocumentClient.PutItemOutput) => {
                let resp: Response = {statusCode: 201, body: JSON.stringify(value.Attributes)};
                resolve(resp);
            }).catch((error: ErrorDynamoDB)=> {
                console.log(error);
                let resp: Response = {statusCode: 500, body: JSON.stringify({message: 'Internal Server Error'})};
                reject(resp);
            });
        });
    }

    static put(tableName: string, object: {}): Promise<Response> {
        const params = {
            TableName: tableName,
            Item: object,
            ReturnValues: 'ALL_OLD'
        };
        console.log(params);
        return new Promise<Response>((resolve, reject) => {
            DocumentClientDB.getDocumentClient().put(params).promise().then((value: DocumentClient.AttributeMap) => {
                console.log(value);
                let resp: Response = {statusCode: 201, body: JSON.stringify({message: 'Success', Item: object})};
                resolve(resp)
            }).catch((error) => {
                console.log(error);
                let resp: Response = {statusCode: 500, body: JSON.stringify({message: 'Internal Server Erro'})};
                reject(resp);
            });
        });
    }

    static get(tableName: string, id: string): Promise<Response> {

        const params = {
            TableName: tableName,
            Key: {id: id}
        };

        return new Promise<Response>((resolve, reject) => {
            DocumentClientDB.getDocumentClient().get(params).promise().then((value: DocumentClient.GetItemOutput) => {
                console.log(value);
                let resp: Response = {statusCode: 201, body: JSON.stringify(value.Item)};
                resolve(resp);
            }).catch((error) => {
                console.log(error);
                let resp: Response = {statusCode: 500, body: JSON.stringify({message: 'Internal Server Error'})};
                reject(resp);
            });
        });
    }

    static delete (tableName: string, id: string): Promise<Response> {

        const params = {
            TableName: tableName,
            Key: {id: id},
            ReturnValues: 'ALL_OLD'
        };

        return new Promise<Response>((resolve, reject) => {
            DocumentClientDB.getDocumentClient().delete(params).promise().then((value: DocumentClient.AttributeMap) => {
                console.log(value);
                let resp: Response = {statusCode: 201, body: JSON.stringify(value.Attributes)};
                resolve(resp);
            }).catch((error) => {
                console.log(error);
                let resp: Response = {statusCode: 500, body: JSON.stringify({message: 'Internal Server Error'})};
                reject(resp);
            });
        });
    }

    static scan (scanInput: DocumentClient.ScanInput): Promise<Response> {
       
        return new Promise<Response> ((resolve, reject)=>{
            co(function* () {
                let scanOutput: DocumentClient.ScanOutput = yield DocumentClientDB.getDocumentClient().scan(scanInput).promise();
                while(scanInput.FilterExpression && scanOutput.LastEvaluatedKey) {
                    let scanOutputInside: DocumentClient.ScanOutput = yield DocumentClientDB.getDocumentClient().scan(scanInput).promise();
                    scanInput.ExclusiveStartKey = scanOutputInside.LastEvaluatedKey;
                    scanOutput.LastEvaluatedKey = scanOutputInside.LastEvaluatedKey;
                    scanOutput.Items = scanOutputInside.Items.concat(scanOutput.Items);
                }

                return scanOutput;
            }).then((scanOutput: DocumentClient.ScanOutput) => {
                return resolve({statusCode: 200, body: JSON.stringify(scanOutput)});
            }, (error) => {
                return reject({statusCode: 500, body: JSON.stringify({message: 'Internal Server Error'})});
            });
        }); 
    }

    static query(queryInput: DocumentClient.QueryInput): Promise<Response> {

        return new Promise<Response>((resolve, reject) => {
            DocumentClientDB.getDocumentClient().query(queryInput).promise().then((value: DocumentClient.QueryOutput) => {
                console.log(value);
                let resp: Response = {statusCode: 201, body: JSON.stringify(value)};
                resolve(resp);
            }).catch((error) => {
                console.log(error);
                let resp: Response = {statusCode: 500, body: JSON.stringify({message: 'Internal Server Error'})};
                reject(resp);
            });
        });
    }

}
