'use strict';

import {SHA1, enc} from 'crypto-js';

export class Util {
    
    static removeSpecialChar(str: string):string {
        if(str) {
            let newStr:string[]
            let accents = 'ÀÁÂÃÄÅàáâãäåßÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
            let accentsOut = "AAAAAAaaaaaaBOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
            newStr= str.split('');
            newStr.forEach((letter, index) => {
            let i = accents.indexOf(letter);
            if (i != -1) {
                newStr[index] = accentsOut[i];
            }
            })
            return newStr.join('').toLocaleLowerCase();
        } else {
            return str;
        }
    }

    static sha1(input){
        return SHA1(JSON.stringify(input)).toString(enc.Hex);
    }

}