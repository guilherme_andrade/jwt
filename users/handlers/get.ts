'use strict';

import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { User } from '../models/User';
import { ErrorDynamoDB } from '../../dynamodb/ErrorDynamoDB';
import { UserRepository } from '../../repositories/UserRepository';
import { Response } from '../../infrastructure/Response';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  if (!event.pathParameters.id) {
    console.error('Validation Failed');
    let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
    cb(null, resp);
    return;
  }

  const user: User = User.deserialize(`{"id": "${event.pathParameters.id}"}`);
  const userRepository: UserRepository = new UserRepository();
  userRepository.findById(user).then((resp: Response) => {
    cb(null, resp);
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
};

export {handler};
