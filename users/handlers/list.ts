'use strict';
import { FilterExpressionBuilder } from './../../dynamodb/FilterExpressionBuilder';
import { Condition } from '../../dynamodb/Condition';
import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { User } from '../models/User';
import { ErrorDynamoDB } from '../../dynamodb/ErrorDynamoDB';
import { UserRepository } from '../../repositories/UserRepository';
import { Response } from '../../infrastructure/Response';

const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {

  let filterExpressionBuilder = new FilterExpressionBuilder(process.env.DYNAMODB_USER_TABLE, 10);
  let queryStringParameters = event.queryStringParameters;
  let exclusiveStartKey : {} = null;
  if(event.queryStringParameters) {
    if(!queryStringParameters.exclusiveStartKey) {
      filterExpressionBuilder.where('searchName', queryStringParameters.searchName, Condition.CONTAINS)
      filterExpressionBuilder.where('searchEmail', event.queryStringParameters.searchEmail, Condition.EQ);
      filterExpressionBuilder.where('role', event.queryStringParameters.role, Condition.EQ);
    } else {
      filterExpressionBuilder.exclusiveStartKey = queryStringParameters.exclusiveStartKey;
    }
  }
  
  const userRepository: UserRepository = new UserRepository();
  userRepository.findByQueryParameters(filterExpressionBuilder).then((resp: Response) => {
    cb(null, resp);
  }).catch((resp: Response)=>{
    cb(null, resp);
  });
};

export {handler};
