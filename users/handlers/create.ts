'use strict';
import { VerifyExistsUser } from './../specification/VerifyExistsUser';
import { APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda'
import { User } from '../models/User';
import { ErrorDynamoDB } from '../../dynamodb/ErrorDynamoDB';
import { Response } from '../../infrastructure/Response';
import { UserRepository } from '../../repositories/UserRepository';
import { Util } from '../../common/Util';


const handler: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  
  console.log(event);
  let user: User = User.deserialize(event.body);

  if (!user.email || !user.password || !user.name) {
    console.error('Validation Failed');
    let resp: Response = {statusCode: 400, body: JSON.stringify({message: 'Bad Request'})};
    cb(null, resp);
    return;
  }

  
  VerifyExistsUser.verify(user.email).then((userVerified: User) => {
    if(userVerified) {
      let resp: Response = {statusCode: 401, body: JSON.stringify({message: 'Unauthorized'})};
      cb(null, resp);
    } else {
      if(user.password) {
        user.password = Util.sha1(user.password);
      }

      const userRepository: UserRepository = new UserRepository();
      userRepository.add(user).then((resp: Response) => {
        cb(null, resp);
      }).catch((resp: Response)=>{
        cb(null, resp);
      });
    }
  });
  

  
};

export {handler};
