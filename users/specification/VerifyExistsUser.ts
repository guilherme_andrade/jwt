import { KeyConditionExpressionBuilder } from './../../dynamodb/KeyConditionExpressionBuilder';
import { ScanInput, DocumentClient } from 'aws-sdk/clients/dynamodb';
import { Condition } from './../../dynamodb/Condition';
import { User } from './../models/User';
import { Callback } from 'aws-lambda';
import { UserRepository } from '../../repositories/UserRepository';
import { Response } from '../../infrastructure/Response';

export class VerifyExistsUser {

    static verify(email: string) : Promise<User> {
        return new Promise<User>((resolve, reject) => {
        const userRepository: UserRepository = new UserRepository();
        let keyConditionExpressionBuilder = new KeyConditionExpressionBuilder(process.env.DYNAMODB_USER_TABLE_EMAIL_INDEX, process.env.DYNAMODB_USER_TABLE)
        .where('searchEmail', email, Condition.EQ);
        userRepository.findByIndex(keyConditionExpressionBuilder).then((resp: Response) => {
            let output: DocumentClient.QueryOutput = JSON.parse(resp.body);
            let user: User = output.Items.length > 0 ? <User>output.Items[0]: null; 
            resolve(user);
            }).catch((resp: Response)=>{
            reject(resp);
            });
        });
    }

}