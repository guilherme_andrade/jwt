import { Callback } from 'aws-lambda';
import { Response } from './../../infrastructure/Response';
import { Entity } from './../../infrastructure/Entity';
import { Model } from './../../infrastructure/Model';
import { VerifyExistsUser } from '../specification/VerifyExistsUser';
import { Util } from '../../common/Util';

@Model(process.env.DYNAMODB_USER_TABLE)
export class User extends Entity {

    private _name: string;
    private _searchName: string;
    private _email: string;
    private _searchEmail: string;
    private _password: string;
    private _role: string;
    private _searchRole: string;
    
    constructor(){super();}
    
    get email(): string {
        return this._email;
    }

    set email(email: string) {
        this._email = email;
    }
    
    get name(): string {
        return this._name;
    }

    set name(name: string) {
        this._name = name;
    }

    get password(): string {
        return this._password;
    }

    set password(password: string) {
        this._password = password;
    }

    get role(): string {
        return this._role;
    }

    set role(role: string) {
        this._role = role;
    }


    get searchName(): string {
        return this._searchName;
    }

    set searchName(searchName: string) {
        this._searchName = searchName;
    }

    get searchEmail(): string {
        return this._searchEmail;
    }

    set searchEmail(searchEmail: string) {
        this._searchEmail = searchEmail;
    }

    public login(email: string, password: string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            VerifyExistsUser.verify(email).then((userVerified: User) => {
                if(!userVerified){
                    reject(new Error('User not found!'));
                } 
                
                if(userVerified && userVerified.password !==  Util.sha1(password)) {
                    reject(new Error('Invalid password'));
                }
                
                resolve(userVerified);
            });
        });
    }
    

    public static deserialize(serialized: string) :User {
        if (!serialized) return null;
        const {id, name, email, password, role} = JSON.parse(serialized);
        let user: User =  new User();
        user.email = email;
        user.name = name;
        user.searchName = Util.removeSpecialChar(name);
        user.searchEmail = Util.removeSpecialChar(email);
        user.password = password;
        user.role = role;
        user.id = id;
        return user;
    }
}