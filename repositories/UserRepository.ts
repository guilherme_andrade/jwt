import {Repository} from '../infrastructure/Repository'
import {User} from '../users/models/User'

export class UserRepository extends Repository<User> {
  
}