import {Repository} from '../infrastructure/Repository'
import {Project} from '../projects/models/Project'

export class ProjectRepository extends Repository<Project> {
  
}