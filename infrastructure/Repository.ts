'use strict';
import { FilterExpressionBuilder } from './../dynamodb/FilterExpressionBuilder';
import { KeyConditionExpressionBuilder } from './../dynamodb/KeyConditionExpressionBuilder';
import { ScanInput } from 'aws-sdk/clients/dynamodb';
import { DynamoDB } from 'aws-sdk';
import { DocumentClientDB } from '../dynamodb/DocumentClientDB';
import { v4 as uuid } from 'uuid';
import { Callback } from 'aws-lambda';
import { ErrorDynamoDB } from '../dynamodb/ErrorDynamoDB';
import { Response } from '../infrastructure/Response';
import { Entity } from './Entity'



export class Repository<T extends Entity> {
    
    constructor(){}


    add(entity: T): Promise<Response> {
        entity.id = uuid();
        return DocumentClientDB.put(entity.tableName, entity.toJSON());
    }
    
    remove(entity: T): Promise<Response> {
        return DocumentClientDB.delete(entity.tableName, entity.id);
    }

    findById(entity: T): Promise<Response>{
        return DocumentClientDB.get(entity.tableName, entity.id);
    }

    update(entity: T): Promise<Response> {
        return DocumentClientDB.update(entity.tableName, entity.toJSON());
    }

    findByIndex(qb: KeyConditionExpressionBuilder): Promise<Response> {
        return DocumentClientDB.query(qb.build());
    }

    
    findByQueryParameters(qb: FilterExpressionBuilder): Promise<Response>{
        return DocumentClientDB.scan(qb.build());
    }

    
}