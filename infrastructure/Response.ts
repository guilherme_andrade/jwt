'use strict';
export interface Response {
    statusCode: number;
    body: string;
    headers?: {'Access-Control-Allow-Origin': '*'}
}