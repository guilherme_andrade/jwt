auth-api
============================

[![serverless](http://public.serverless.com/badges/v3.svg)](http://www.serverless.com)

## Requirements
* Java Runtime Engine (JRE) version 6.x or newer to run dynamodb locally.


## How to develop and test offline?
We have used [serverless-offline plugin](https://github.com/dherault/serverless-offline) and [serverless-dynamodb-local plugin](https://github.com/99xt/serverless-dynamodb-local). You can declare your table templates and seeds in `offline/migrations/` folder just like the `todo.json` template. When you spin up the offline server, these tables will be used as the datasources for your lambda functions.

## Production vs Offline
Thanks to the offline plugin's environment variable `IS_OFFLINE` we can select between local dynamodb and aws dynamodb. 
```
var dynamodbOfflineOptions = {
        region: "localhost",
        endpoint: "http://localhost:8000"
    },
    isOffline = () => process.env.IS_OFFLINE;

var client = isOffline() ? new AWS.DynamoDB.DocumentClient(dynamodbOfflineOptions) :  new AWS.DynamoDB.DocumentClient();
```

## Installation & Usage

* Install VSC 
  ```
  https://code.visualstudio.com/download
  ```
* Install o NVM 
  ```
  https://github.com/creationix/nvm
  ```
* Install client aws 
  ```
  https://docs.aws.amazon.com/pt_br/cli/latest/userguide/installing.html
  ```
* Install PostMan 
  ```
  https://blog.bluematador.com/posts/postman-how-to-install-on-ubuntu-1604
  ```
* Install serverless
  ```
  npm i -g serverless
  ```
* Install project dependencies. `cd api-domain` and type,
```
  npm install 
```
* Install dynamodb local. (Make sure you have Java runtime 6.x or newer)
```
  npm run db-setup
```

## Deploying to AWS
When you are ready to deploy your database and api to AWS, you can create multiple 
APIGateways for different service level stages. For example you can create "dev" and "production" stages.
When you deploy to a specific stage, it will create a separate database tables for that stage.

Following command will deploy your local dabase and local API Gateway to AWS stage.
```
cd serverless
serverless deploy --stage <stage> --verbose
```

## Links
* [serverless-dynamodb-local plugin](https://github.com/99xt/serverless-dynamodb-local)
* [serverless-offline plugin](https://github.com/dherault/serverless-offline)

